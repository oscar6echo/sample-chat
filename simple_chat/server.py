
import logging

import flask

from flask import Flask, render_template, redirect, url_for
from flask_socketio import SocketIO, join_room, leave_room, send


logging.getLogger().setLevel(logging.DEBUG)


app = Flask(__name__, template_folder='templates')
app.config['DEBUG'] = True

socketio = SocketIO(app, json=flask.json)

clients = []


@socketio.on('join', namespace='/chat')
def on_join(data):
    print(data)
    name = data.get('name', None)
    room = data.get('room', None)

    if not name or not room:
        msg = 'Wrong join: ' + str(data)
        print(msg)
        return {'msg': msg}

    join_room(name)
    join_room(room)
    clients.append([name, room])
    status = '{} has joined chat room {}'.format(name, room)
    socketio.emit('status', {'text': status}, room=room, namespace='/chat')
    print('connected clients =', str(clients))
    return {'text': status}


@socketio.on('leave', namespace='/chat')
def on_leave(data):
    print(data)
    name = data.get('name', None)
    room = data.get('room', None)

    if not name or not room:
        msg = 'Wrong leave: ' + str(data)
        print(msg)
        return {'msg': msg}

    clients = a = [c for c in clients if c != [name, room]]
    leave_room(name)
    leave_room(room)
    status = '{} has left chat room {}'.format(name, room)
    socketio.emit('msg', {'text': status}, room=room, namespace='/chat')
    print('connected clients =', str(clients))
    return {'text': status}


@socketio.on('msg', namespace='/chat')
def on_msg(data):
    print(data)
    name = data.get('name', None)
    room = data.get('room', None)
    text = data.get('text', None)

    if (not name) or( not room) or (not text):
        msg = 'Wrong msg:' + str(data)
        print(msg)
        return {'msg': msg}

    socketio.emit('status', {'text': text, 'name': name}, room=room, namespace='/chat')
    msg = 'msg "{}" broadcasted to room {}'.format(text, room)
    return {'msg': msg}



# default route
@app.route('/doc', methods=['GET'])
def main():
    """single page app"""
    return render_template('main.html')


# fallback
@app.route('/')
def root():
    """redirect"""
    return redirect(url_for('main'))


@app.route('/<path:dummy>')
def fallback(dummy):
    """redirect"""
    return redirect(url_for('main'))


logging.info('InitApp')

