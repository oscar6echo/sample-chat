conda create -y --name chat python=3
source activate chat
pip install flask flask-socketio
conda env export > env.yml
deactivate chat


# conda env create -f env.yml